
import RealmSwift

let DB = DBModule.sharedInstance

class DBModule {
    static let sharedInstance = DBModule()
    
    fileprivate let realm = try! Realm()
    fileprivate let intervals = try! Realm().objects(Interval.self).sorted(byKeyPath: "beginTime", ascending: true)
}

// MARK: Write data
extension DBModule {
    
    // Save an interval to DB
    @discardableResult
    func writeToDB(newInterval: Interval) -> Bool {
        let result = intervals.filter("beginTime == %@", newInterval.beginTime)
        if result.isEmpty {
            try! self.realm.write {
                self.realm.add(newInterval)
            }
            return true
        }
        return false
    }
}

// MARK: Get data
extension DBModule {
    // Return array of days (start of the day) from all intervals
    func getArrayOfTrainingDays() -> [Date] {
        let allTrainingDates = (intervals.value(forKey: "beginTime") as! [Date]).map { NSCalendar.current.startOfDay(for: $0) }
        return Array(Set(allTrainingDates)).sorted()
    }
    
    // Return all intervals of today
    func getTodayTrainingIntervals() -> Results<Interval> {
        return getIntervalsInDate(Date(), withStateFilter: .train)
    }
    
    // Return all intervals filtered by day and state
    func getIntervalsInDate(_ date: Date, withStateFilter state: State) -> Results<Interval> {
        return getIntervalsInDate(date).filter("state == %@", state.rawValue)
    }
}

// MARK: private functions
private extension DBModule {
    func getIntervalsInDate(_ date: Date) -> Results<Interval> {
        let dayStart = NSCalendar.current.startOfDay(for: date)
        let dayEnd: Date = {
            var components = DateComponents()
            components.day = 1
            components.second = -1
            return NSCalendar.current.date(byAdding: components, to: dayStart)!
        }()
        
        return intervals.filter("beginTime BETWEEN %@", [dayStart, dayEnd])
    }
}
